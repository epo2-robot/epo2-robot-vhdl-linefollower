-- Date: 15 May 2017
-- Authors
-- Sebastian Jordan, student number: 4590813
-- Mathijs van Geerenstein, student number: 4598660

library IEEE;
use IEEE.std_logic_1164.all;

entity inputbuffer is
	port (	clk		: in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end entity inputbuffer;

architecture behavioural of inputbuffer is
signal sensor_l_tus, sensor_m_tus, sensor_r_tus : std_logic;
signal sensor_l_buf, sensor_m_buf, sensor_r_buf : std_logic;

begin
  process (clk)
    begin
      if (clk'event and clk = '1') then
        sensor_l_tus <= sensor_l_in;
        sensor_m_tus <= sensor_m_in;
        sensor_r_tus <= sensor_r_in;
        sensor_l_buf <= sensor_l_tus;
        sensor_m_buf <= sensor_m_tus;
        sensor_r_buf <= sensor_r_tus;
      end if;
    end process;
    
  sensor_l_out <= sensor_l_buf;
  sensor_m_out <= sensor_m_buf;
  sensor_r_out <= sensor_r_buf;
  end architecture behavioural;
  