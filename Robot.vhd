-- Date: 15 May 2017
-- Authors
-- Sebastian Jordan, student number: 4590813
-- Mathijs van Geerenstein, student number: 

library IEEE;
use IEEE.std_logic_1164.all;

entity FPGA is
port (	        clk		: in	std_logic;
		reset	        : in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		pwmL		: out	std_logic;
		pwmR		: out	std_logic
	);
end entity FPGA;

architecture structural of FPGA is

component inputbuffer is
	port (	clk		: in	std_logic;

		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end component inputbuffer;

component controller is
	port (	clk			: in	std_logic;
		reset			: in	std_logic;

		sensor_l		: in	std_logic;
		sensor_m		: in	std_logic;
		sensor_r		: in	std_logic;

		count_in		: in	std_logic_vector (19 downto 0);
		count_reset		: out	std_logic;

		motor_l_reset		: out	std_logic;
		motor_l_direction	: out	std_logic;

		motor_r_reset		: out	std_logic;
		motor_r_direction	: out	std_logic
	);
end component controller;

component timebase is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;

		count_out	: out	std_logic_vector (19 downto 0)
	);
end component timebase;

component motorcontrol is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		direction	: in	std_logic;
		count_in	: in	std_logic_vector (19 downto 0);

		pwm		: out	std_logic
	);
end component motorcontrol;

-- Needed intermediate signals
signal sensorL,sensorM,sensorR, countRS , M_L_RS, M_L_D, M_R_RS, M_R_D : std_logic;
signal count : std_logic_vector (19 downto 0);

begin 

LB1: inputbuffer port map ( clk => clk, sensor_l_in => sensor_l_in , sensor_m_in => sensor_m_in  ,sensor_r_in => sensor_r_in,
			    sensor_l_out => sensorL , sensor_m_out => sensorM , sensor_r_out => sensorR		    );

LB2: controller port map ( clk => clk , reset => reset , sensor_l => sensorL , sensor_m => sensorM , sensor_r => sensorR , count_in => count , count_reset=>countRS ,
			   motor_l_reset => M_L_RS , motor_l_direction =>  M_L_D , motor_r_reset => M_R_RS , motor_r_direction => M_R_D				   );

LB3: timebase port map ( clk => clk , reset => countRS , count_out => count);

MCL: motorcontrol port map ( clk => clk , reset => M_L_RS , direction => M_L_D , count_in => count , pwm=>pwmL ) ;

MCR:  motorcontrol port map ( clk => clk , reset => M_R_RS , direction => M_R_D , count_in => count , pwm=>pwmR ) ;

end architecture structural ;

