LIBRARY IEEE ;
use IEEE.std_logic_1164.all;


entity inputbuffer_testbench is
end entity inputbuffer_testbench  ;

architecture structural of inputbuffer_testbench  is
component inputbuffer is
	port (	clk		: in	std_logic;
		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;

		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end component inputbuffer;

signal clk , sensor_l_in , sensor_m_in , sensor_r_in : std_logic;
signal sensor_l_out, sensor_m_out ,sensor_r_out : std_logic;

begin
	clk <= '1' after 0 ns ,
	       '0' after 10 ns when clk /= '0' else '1' after 10 ns ;
	
	sensor_l_in <= '0' after 0 ns ,
		       '1' after 15 ns ,
		       '0' after 25 ns ;
	sensor_m_in <= '0' after 0 ns ,
		       '1' after 5 ns ,
		       '0' after 50 ns ;
	sensor_r_in <= '0' after 0 ns ,
		       '1' after 5 ns ,
		       '0' after 25 ns ;

	

LB1: inputbuffer port map ( clk => clk ,  sensor_l_in => sensor_l_in , sensor_m_in => sensor_m_in , sensor_r_in => sensor_r_in ,
			    sensor_l_out => sensor_l_out , sensor_m_out => sensor_m_out , sensor_r_out => sensor_r_out );

end architecture structural ;





