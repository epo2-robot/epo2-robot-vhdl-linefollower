-- Date: 15 May 2017
-- Authors
-- Sebastian Jordan, student number: 4590813
-- Mathijs van Geerenstein, student number: 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity timebase is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;

		count_out	: out	std_logic_vector (19 downto 0) -- 20 bits needed to count to 1000000
	);
end entity timebase;

architecture behavioural of timebase is
  signal count, new_count  : unsigned (19 downto 0);
  
begin
  process (clk)
    begin
      if (clk'event and clk = '1') then -- Act on rising edge
        if (reset = '1') then
          count <= (others => '0'); -- Count value is reset after a reset signal
        else
          count <= new_count;
        end if;
      end if;
    end process;
    
  process (reset, count)
    begin
      if (reset = '0') then
        new_count <= count + 1; -- Only count when reset is '0'
      else 
        new_count <= count;
      end if;
    end process;
    
    count_out <= std_logic_vector(count);
end architecture behavioural;
        