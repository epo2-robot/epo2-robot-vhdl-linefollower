--Authors: 
--Mohammed Abo Alainein   4570081
--Nick Hogendoorn         4567870



library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity controller is
	port (	clk			: in	std_logic;
		reset			: in	std_logic;

		sensor_l		: in	std_logic;
		sensor_m		: in	std_logic;
		sensor_r		: in	std_logic;

		count_in		: in	std_logic_vector (19 downto 0);
		count_reset		: out	std_logic;

		motor_l_reset		: out	std_logic;
		motor_l_direction	: out	std_logic;

		motor_r_reset		: out	std_logic;
		motor_r_direction	: out	std_logic
	);
end entity controller;

-- With 3 sensors we have 8 possible values
-- Black = 0 , White = 1 --
--       Sensors                                 Motor    
-- left   middle    right        left  reset_L        right - reset_R
-- 0      0         0              0      0    	       0        0 		Sharp Left
-- 0      0         1              -      1            0   	0		Gentle Left
-- 0      1         0              1      0            0        0  		Forwards
-- 0      1         1              0      0            0        0 		sharp Left
-- 1      0         0              1      0            -        1 		Gentle Right
-- 1      0         1              1      0            0        0 		Forwards
-- 1      1         0              1      0            1        0 		Sharp Right
-- 1      1         1              1      0            0        0		Forwards


architecture behavioral of controller is 

type controller_state is ( reset_state , line_state , gentle_R , gentle_L , sharp_R , sharp_L, state_Black , state_White , state_M_White ) ;

signal state , next_state : controller_state ;
signal  T : unsigned (19 downto 0) ;
constant T1 : unsigned (19 downto 0) := to_unsigned ( 999998 , 20 ) ;

begin 
	T <= unsigned ( count_in ) ;

	process (clk , reset)
	begin
		
			if ( reset = '1' ) then 
				state <= reset_state ;
			elsif ( rising_edge(clk) ) then
				state <= next_state ;
			end if ;
	end process ;


	process ( state , T ,sensor_l , sensor_m , sensor_r)
	begin
		case state is

			when reset_state =>
				motor_l_reset <= '1' ;
				motor_r_reset <= '1' ;
				count_reset   <= '1' ;

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				 if ( (sensor_l = '1' ) and ( sensor_m = '0' ) and (sensor_r = '1')  ) then
					next_state <=  line_state ;				
				elsif ( (sensor_l = '0' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					next_state <=  sharp_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '0')  ) then
					next_state <=  sharp_R ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '0' ) and (sensor_r = '1')  ) then
					next_state <=  gentle_L ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '0' ) and (sensor_r = '0')  ) then
					next_state <=  gentle_R ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '1' ) and (sensor_r = '0')  ) then
					next_state <=  state_M_White ;
				elsif ( (sensor_l = '1' ) and ( sensor_m = '1' ) and (sensor_r = '1')  ) then
					next_state <=  state_White ;
				elsif ( (sensor_l = '0' ) and ( sensor_m = '0' ) and (sensor_r = '0')  ) then
					next_state <=  state_Black ;
				else
					next_state <=  reset_state ;
				end if ;

			when line_state => 
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= line_state ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;

			when gentle_R =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '1' ;
				count_reset   <= '0' ;

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= gentle_R ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;

			when gentle_L =>
				motor_l_reset <= '1' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= gentle_L ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;	
	
			when sharp_R =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '1' ;
				motor_r_direction <= '1' ;

				if (  T < T1  ) then
					next_state <= sharp_R ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;	
	
			when sharp_L =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= sharp_L ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;		

			when state_Black  => --- sharp left --
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '0' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= state_Black ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;		

			when state_White  =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= state_White ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;
			when state_M_White  =>
				motor_l_reset <= '0' ;
				motor_r_reset <= '0' ;
				count_reset   <= '0' ;

				motor_l_direction <= '1' ;
				motor_r_direction <= '0' ;

				if (  T < T1  ) then
					next_state <= state_M_White ;
				elsif (  T >=  T1  ) then
					next_state <= reset_state ;
				end if;		
					

		end case ;
	end process ;
end architecture behavioral ;