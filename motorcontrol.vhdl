-- Date: 15 May 2017
-- Authors
-- Sebastian Jordan, student number: 4590813
-- Mathijs van Geerenstein, student number: 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity motorcontrol is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		direction	: in	std_logic;
		count_in	: in	std_logic_vector (19 downto 0);

		pwm		: out	std_logic
	);
end entity motorcontrol;

architecture behavioural of motorcontrol is

type motor_controller_state is(motor_uit,
     motor_aan, motor_reset);
signal state, new_state : motor_controller_state;
signal T: unsigned (19 downto 0);
constant T1 : unsigned (19 downto 0) := to_unsigned (50000, 20); -- To create a 1 ms pulse
constant T2 : unsigned (19 downto 0) := to_unsigned (100000,20); -- To create a 2 ms pulse

begin
  T <= unsigned (count_in);
  -- Change states
  process (clk)
    begin
      if (rising_edge (clk)) then
        if (reset = '1') then
          state <= motor_reset;
        else 
          state <= new_state;
        end if;
      end if;
  end process;

  process (state, T)
   begin
   case state is
     when motor_reset =>
       pwm <= '0';
       new_state <= motor_aan;
     when motor_aan =>
       pwm <= '1';
       if( (direction = '0' and T > T1) or (direction = '1' and T > T2)) then -- Pulse length according to direction signal
          new_state <= motor_uit;
       else
          new_state <= motor_aan ;
       end if;
     when motor_uit =>
       pwm <= '0';
       new_state <= motor_uit;
   end case;
  end process;
end architecture behavioural;